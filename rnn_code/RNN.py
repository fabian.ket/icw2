import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
from abc import ABC, abstractmethod
import logging
import rnn_code.reberGrammar as rg  # i have to import it that way, so it will work in the notebooks
import os

from IPython.display import clear_output

logging.getLogger('tensorflow').disabled = True


def get_rg_sample_accuracy(rnn=None, n=100, end_count=1, verbose=1):

    sample_batch = 1000 if n >= 1000 else n

    words = []
    in_grammar = []

    start_sequence = np.asarray([[1., 0., 0., 0., 0., 0., 0.]], dtype=np.float32)
    end_sequence = np.asarray([[0., 0., 0., 0., 0., 0., 1.]], dtype=np.float32)

    if end_count <= 1:
        grammar_tester = rg.in_grammar
    else:
        grammar_tester = rg.in_embedded_grammar

    acc = 0
    _n = n

    start_time = time.time()

    while _n > 0:
        _n -= sample_batch

        if _n < 0:
            sample_batch += _n

        samples = rnn.sample(start_sequence, end_sequence, n=sample_batch, end_count=end_count)

        for sample in samples:
            word = rg.sequenceToWord(sample)
            words.append(word)
            in_grammar.append(grammar_tester(word))

        acc = sum(in_grammar) / len(in_grammar)

        if verbose == 1:
            print("Sample acc: {}% over {} samples".format(acc * 100, len(in_grammar)))

    if verbose == 1:
        print("Duration: {}sec".format(time.time() - start_time))

    return acc, words


class AbstractRNN(ABC):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        self.name = "_AbstractRNN"

        # GRAPH STUFF
        self.n_in  = n_in
        self.n_hid = n_hid
        self.n_out = n_out
        self.learning_rate = learning_rate
        self.epsilon = epsilon

        self.tf_graph = tf.Graph()
        self.tf_config = None
        self.tf_init = None
        self.tf_summaries = None

        self.tf_x = None
        self.tf_y = None

        self.tf_out_t = None

        self.tf_cost = None
        self.tf_optimize = None

        # NON GRAPH STUFF
        self.train_cost_list = []
        self.save_file = "abstract_rnn.chkp"

    @abstractmethod
    def rnn_cell(self, _out, x_t):
        pass

    def train(self, x_train, y_train, epochs=20, load=False):
        """
        Trains the RNN with the given x_train & y_train over epochs epochs and saves it under self.save_file
        Uses online training, so trains with only one example at a time
        :param x_train: np.array(shape=(n,m)); list of n training examples
        :param y_train: np.array(shape=(n.m)); list of n training labels
        :param epochs: Int, number of epochs
        :param load: Bool; if set loads old trained model
        """
        
        n_examples = x_train.shape[0]

        start_time = time.time()
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()

            if load:  # restores old trained model
                saver.restore(sess, self.save_file)

            writer = tf.summary.FileWriter('logs', sess.graph, filename_suffix=self.name)
            
            for i in range(epochs):
                if i % 10 == 0:
                    clear_output(wait=True)
            
                train_cost = 0.0
                
                print("Epoch: {:3} / {:3} | ".format(i+1, epochs), end="")
                
                #for j in range(n_examples // batch_size):
                #    x = x_train[j * batch_size : (j+1) * batch_size]
                #    y = y_train[j * batch_size : (j+1) * batch_size]
                # 
                #    summ, _, cost = \
                #        sess.run([self.tf_summaries, self.tf_optimize, self.tf_cost], feed_dict={self.tf_x: x, self.tf_y: y})
                #    train_cost += cost
                
                j = 0
                
                for x, y in zip(x_train, y_train):
                    x = np.asarray(x)
                    y = np.asarray(y)
                    
                    summ, _, cost = \
                        sess.run([self.tf_summaries, self.tf_optimize, self.tf_cost], feed_dict={self.tf_x: x, self.tf_y: y})
                    train_cost += cost
                    
                    j +=1
                    
                    if j % (n_examples // 10) == 0:
                        print("{}% ".format(j / n_examples * 100), end="")

                writer.add_summary(summ, global_step=i)
                self.train_cost_list.append(train_cost)
                print("| Cost: {} ".format(train_cost))
                
                if (i+1) % 5 == 0:
                    saver.save(sess, self.save_file)
                    print("\nModel saved! ({})\n".format(self.save_file))

            saver.save(sess, self.save_file)
            print("Model saved! ({})".format(self.save_file))
        
        print("")
        print("Duration: {}sec".format(time.time() - start_time))

    def predict(self, x):
        """
        Predicts the output for the given x
        :param x: np.array(shape=(1,N))
        """
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()
            saver.restore(sess, self.save_file)
            out = sess.run([self.tf_out_t], feed_dict={self.tf_x: x})

        return np.asarray(out)

    def sample(self, start_sequence, end_sequence, end_count=1, n=1):
        """
        Creates samples with the RNN
        :param start_sequence: np.array(shape=(1,N))
        :param end_sequence: np.array(shape=(1,N))
        :param end_count: Int; times the end_sequence has to be outputted, used for embedded reber grammar
        :param n: Int; number of samples
        :return np.array(shape=(n,m)); returns n samples with different m lengths
        """
        
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()
            saver.restore(sess, self.save_file)

            out_samples = []
            indices = np.arange(self.n_out)
            
            for i in range(n):
                h = np.zeros(self.n_hid, dtype=np.float32)
                out = start_sequence
            
                out_sequences = [out]
                count = 0
                loop = True
                
                while loop:
                    h, out = sess.run([self.tf_h_t, self.tf_out_t], feed_dict={self.tf_x: out, self.tf_h_tm1: h})                    
                    h = np.reshape(h, self.n_hid)

                    p = out[0] / out[0].sum()

                    choice = np.random.choice(indices, p=p)
                    out[0][:] = 0.0
                    out[0][choice] = 1.0

                    out_sequences.append(out[0])

                    if (out == end_sequence).all():
                        count += 1

                    loop = not (count == end_count)

                out_samples.append(np.asarray(out_sequences))

        return np.asarray(out_samples)
        

    def plot(self):
        """
        Plots the training cost
        """
        plt.plot(self.train_cost_list)
        plt.xlabel("Epochs")
        plt.ylabel("Cost")
        plt.show()

    def debug(self):
        pass


class RNN(AbstractRNN):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        self.name = "_RNN"

        # NON GRAPH STUFF
        self.save_file = os.path.join(os.path.dirname(__file__), "../tf_save_rnn/rnn.chkp")

        # TF GRAPH
        with self.tf_graph.as_default():
            self.tf_x = tf.placeholder(shape=(None, self.n_in), dtype=tf.float32, name="INPUT")
            self.tf_y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float32, name="LABEL")

            self.tf_W_ih = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ih")
            self.tf_W_hh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_hh")
            self.tf_b_h  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_h")

            self.tf_W_ho = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_out)), dtype=tf.float32, name="W_ho")
            self.tf_b_o  = tf.Variable(tf.zeros([self.n_out]), dtype=tf.float32, name="b_o")
            
            self.tf_h_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), 
                                                        shape=(self.n_hid), name="h_tm1")
                                                        
            self.tf_out_t = tf.constant(np.zeros(self.n_out, dtype=np.float32))

            (self.tf_h_t, self.tf_out_t) = tf.scan(self.rnn_cell, self.tf_x, initializer=(self.tf_h_tm1, self.tf_out_t))

            self.tf_loss = -tf.reduce_mean(
                self.tf_y * tf.log(self.tf_out_t + self.epsilon) +
                (1. - self.tf_y) * tf.log(1. - self.tf_out_t + self.epsilon)
            )
            
            self.tf_cost = self.tf_loss
            self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.tf_cost)
            self.tf_optimize = self.tf_optimizer

            # Weights & bias histogram
            tf.summary.histogram("W_ih", self.tf_W_ih)
            tf.summary.histogram("W_hh", self.tf_W_hh)
            tf.summary.histogram("b_h", self.tf_b_h)

            tf.summary.histogram("W_ho", self.tf_W_ho)
            tf.summary.histogram("b_o", self.tf_b_o)

            self.tf_config = tf.ConfigProto()
            self.tf_config.gpu_options.allow_growth = True
            self.tf_init = tf.global_variables_initializer()
            self.tf_summaries = tf.summary.merge_all()

    def rnn_cell(self, _out, x_t):
        (h_tm1, _) = _out
        h_t = tf.tanh(tf.matmul(tf.expand_dims(x_t, 0),   self.tf_W_ih) +
                      tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_hh) + self.tf_b_h)
        out_t = tf.sigmoid(tf.matmul(h_t, self.tf_W_ho) + self.tf_b_o)

        return tf.reshape(h_t, [self.n_hid]), tf.reshape(out_t, [self.n_out])


class LSTM(AbstractRNN):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        self.name = "_LSTM"

        # NON GRAPH STUFF
        self.save_file = os.path.join(os.path.dirname(__file__), "../tf_save_lstm/lstm.chkp")

        # TF GRAPH
        with self.tf_graph.as_default():
            self.tf_x = tf.placeholder(shape=(None, self.n_in), dtype=tf.float32, name="INPUT")
            self.tf_y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float32, name="LABEL")

            self.tf_W_fx = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_fx")
            self.tf_W_fh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_fh")
            self.tf_b_f  = tf.Variable(tf.ones([self.n_hid]), dtype=tf.float32, name="b_f")

            self.tf_W_ix = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ix")
            self.tf_W_ih = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_ih")
            self.tf_b_i  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_i")

            self.tf_W_cx = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_cx")
            self.tf_W_ch = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_ch")
            self.tf_b_c  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_c")

            self.tf_W_ox = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ox")
            self.tf_W_oh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_oh")
            self.tf_b_o  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_o")

            self.tf_W_outh = tf.Variable(tf.truncated_normal(shape=(n_hid, n_out)), dtype=tf.float32, name="W_outh")
            self.tf_b_out  = tf.Variable(tf.zeros([n_out], dtype=tf.float32, name="b_out"))

            self.tf_h_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), shape=(self.n_hid), name="h_tm1")
            self.tf_C_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), shape=(self.n_hid), name="C_tm1")
            self.tf_out_t = tf.constant(np.zeros(self.n_out, dtype=np.float32))

            (self.tf_h_t, self.tf_C_t, self.tf_out_t) = tf.scan(self.rnn_cell, self.tf_x, initializer=(self.tf_h_tm1, self.tf_C_tm1, self.tf_out_t))

            self.tf_loss = -tf.reduce_mean(
                self.tf_y * tf.log(self.tf_out_t + self.epsilon) +
                (1. - self.tf_y) * tf.log(1. - self.tf_out_t + self.epsilon)
            )

            self.tf_cost = self.tf_loss
            self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.tf_cost)
            self.tf_optimize = self.tf_optimizer
            

            # Weights & bias histogram
            tf.summary.histogram("W_fx", self.tf_W_fx)
            tf.summary.histogram("W_fh", self.tf_W_fh)
            tf.summary.histogram("b_f", self.tf_b_f)

            tf.summary.histogram("W_ix", self.tf_W_ix)
            tf.summary.histogram("W_ih", self.tf_W_ih)
            tf.summary.histogram("b_i", self.tf_b_i)

            tf.summary.histogram("W_cx", self.tf_W_cx)
            tf.summary.histogram("W_ch", self.tf_W_ch)
            tf.summary.histogram("b_c", self.tf_b_c)

            tf.summary.histogram("W_ox", self.tf_W_ox)
            tf.summary.histogram("W_oh", self.tf_W_oh)
            tf.summary.histogram("b_o", self.tf_b_o)

            tf.summary.histogram("W_outh", self.tf_W_outh)
            tf.summary.histogram("b_out", self.tf_b_out)

            self.tf_config = tf.ConfigProto()
            self.tf_config.gpu_options.allow_growth = True
            self.tf_init = tf.global_variables_initializer()
            self.tf_summaries = tf.summary.merge_all()

    def rnn_cell(self, _out, x_t):
        """
        LSTM cell implementation, based on:
        https://colah.github.io/posts/2015-08-Understanding-LSTMs/

        :param _out (h_t, c_t, out_t)
        :param x_t  single train sequence

        :return (h_t, c_t, out_t)
        """

        h_tm1, c_tm1, _ = _out

        f_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_fx) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_fh) +
                         self.tf_b_f)

        i_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_ix) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_ih) +
                         self.tf_b_i)

        cc_t = tf.tanh(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_cx) +
                       tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_ch) +
                       self.tf_b_c)

        c_t = tf.multiply(c_tm1, f_t) + tf.multiply(cc_t, i_t)

        o_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_ox) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_oh) +
                         self.tf_b_o)

        h_t = tf.multiply(tf.tanh(c_t), o_t)

        out_t = tf.sigmoid(tf.matmul(h_t, self.tf_W_outh) + self.tf_b_out)

        return tf.reshape(h_t, [self.n_hid]), tf.reshape(c_t, [self.n_hid]), tf.reshape(out_t, [self.n_out])
        
        
    def sample(self, start_sequence, end_sequence, end_count=1, n=1):
        """
        Creates samples with the RNN
        :param start_sequence: np.array(shape=(1,N))
        :param end_sequence: np.array(shape=(1,N))
        :param end_count: Int; times the end_sequence has to be outputted, used for embedded reber grammar
        :param n: Int; number of samples
        :return np.array(shape=(n,m)); returns n samples with different m lengths
        """
        
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()
            saver.restore(sess, self.save_file)

            out_samples = []
            indices = np.arange(self.n_out)
            
            for i in range(n):
                h = np.zeros(self.n_hid, dtype=np.float32)
                C = np.zeros(self.n_hid, dtype=np.float32)
                out = start_sequence
            
                out_sequences = [out]
                count = 0
                loop = True
                
                while loop:
                    h, C, out = sess.run([self.tf_h_t, self.tf_C_t, self.tf_out_t], feed_dict={self.tf_x: out, self.tf_h_tm1: h, self.tf_C_tm1: C})                    
                    h = np.reshape(h, self.n_hid)
                    C = np.reshape(C, self.n_hid)

                    p = out[0] / out[0].sum()

                    choice = np.random.choice(indices, p=p)
                    out[0][:] = 0.0
                    out[0][choice] = 1.0

                    out_sequences.append(out[0])

                    if (out == end_sequence).all():
                        count += 1

                    loop = not (count == end_count)

                out_samples.append(np.asarray(out_sequences))

        return np.asarray(out_samples)


class GRU(AbstractRNN):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        self.name = "_GRU"

        # NON GRAPH STUFF
        self.save_file = os.path.join(os.path.dirname(__file__), "../tf_save_gru/gru.chkp")

        # TF GRAPH
        with self.tf_graph.as_default():
            self.tf_x = tf.placeholder(shape=(None, self.n_in), dtype=tf.float32, name="INPUT")
            self.tf_y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float32, name="LABEL")

            self.tf_W_ux = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ux")
            self.tf_W_uh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_uh")
            self.tf_b_u  = tf.Variable(tf.ones([self.n_hid]), dtype=tf.float32, name="b_u")

            self.tf_W_rx = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_rx")
            self.tf_W_rh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_rh")
            self.tf_b_r  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_r")

            self.tf_W_hcx = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_hcx")
            self.tf_W_hch = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_hch")
            self.tf_b_hc  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_hc")

            self.tf_W_outh = tf.Variable(tf.truncated_normal(shape=(n_hid, n_out)), dtype=tf.float32, name="W_outh")
            self.tf_b_out  = tf.Variable(tf.zeros([n_out], dtype=tf.float32, name="b_out"))

            self.tf_h_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), shape=(self.n_hid), name="h_tm1")
            self.tf_out_t = tf.constant(np.zeros(self.n_out, dtype=np.float32))

            (self.tf_h_t, self.tf_out_t) = tf.scan(self.rnn_cell, self.tf_x, initializer=(self.tf_h_tm1, self.tf_out_t))

            self.tf_loss = -tf.reduce_mean(
                self.tf_y * tf.log(self.tf_out_t + self.epsilon) +
                (1. - self.tf_y) * tf.log(1. - self.tf_out_t + self.epsilon)
            )

            self.tf_cost = self.tf_loss
            self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.tf_cost)
            self.tf_optimize = self.tf_optimizer

            # Weights & bias histogram
            tf.summary.histogram("W_ux", self.tf_W_ux)
            tf.summary.histogram("W_uh", self.tf_W_uh)
            tf.summary.histogram("b_u", self.tf_b_u)

            tf.summary.histogram("W_rx", self.tf_W_rx)
            tf.summary.histogram("W_rh", self.tf_W_rh)
            tf.summary.histogram("b_r", self.tf_b_r)

            tf.summary.histogram("W_hcx", self.tf_W_hcx)
            tf.summary.histogram("W_hch", self.tf_W_hch)
            tf.summary.histogram("b_hc", self.tf_b_hc)

            tf.summary.histogram("W_outh", self.tf_W_outh)
            tf.summary.histogram("b_out", self.tf_b_out)

            self.tf_config = tf.ConfigProto()
            self.tf_config.gpu_options.allow_growth = True
            self.tf_init = tf.global_variables_initializer()
            self.tf_summaries = tf.summary.merge_all()

    def rnn_cell(self, _out, x_t):
        """
        GRU cell implementation, based on:
        https://colah.github.io/posts/2015-08-Understanding-LSTMs/

        :param _out (h_t, out_t)
        :param x_t  single train sequence

        :return (h_t, c_t, out_t)
        """

        h_tm1, _ = _out

        u_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_ux) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_uh) +
                         self.tf_b_u)

        r_t = tf.sigmoid(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_rx) +
                         tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_rh) +
                         self.tf_b_r)

        hc_t = tf.tanh(tf.matmul(tf.expand_dims(x_t, 0), self.tf_W_hcx) +
                       tf.matmul(tf.multiply(tf.expand_dims(h_tm1, 0), r_t), self.tf_W_hch) +
                       self.tf_b_hc)

        h_t = tf.multiply(u_t, hc_t) + tf.multiply(1 - u_t, h_tm1)

        out_t = tf.sigmoid(tf.matmul(h_t, self.tf_W_outh) + self.tf_b_out)

        return tf.reshape(h_t, [self.n_hid]), tf.reshape(out_t, [self.n_out])

class Attention(AbstractRNN):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        self.name = "_ATTENTION"

        # NON GRAPH STUFF
        self.save_file = os.path.join(os.path.dirname(__file__), "../tf_save_attention/attention.chkp")

        # TF GRAPH
        with self.tf_graph.as_default():
            self.tf_x = tf.placeholder(shape=(None, self.n_in), dtype=tf.float32, name="INPUT")
            self.tf_y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float32, name="LABEL")
            
            
            self.factor = 10
            
            
            # ********************************************************
            #                       RNN Start ...
            # ********************************************************
            self.tf_W_ih = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid * self.factor)), dtype=tf.float32, name="W_ih")
            self.tf_W_hh = tf.Variable(tf.truncated_normal(shape=(self.n_hid * self.factor, self.n_hid * self.factor)), dtype=tf.float32, name="W_hh")
            self.tf_b_h  = tf.Variable(tf.zeros([self.n_hid * self.factor]), dtype=tf.float32, name="b_h")

            # self.tf_W_ha = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_ha")
            # self.tf_b_a  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_a")
            
            self.tf_h_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid * self.factor, dtype=np.float32), 
                                                        shape=(self.n_hid * self.factor), name="h_tm1")
                                                        
            # self.tf_a = tf.constant(np.zeros(self.n_hid, dtype=np.float32))
            # ********************************************************
            #                        ... RNN End
            # ********************************************************
            
            
            self.tf_h_t = tf.scan(self.a_cell, self.tf_x, initializer=(self.tf_h_tm1))
            
            
            # ********************************************************
            #                       RNN Start ...
            # ********************************************************
            self.tf_W_CS = tf.Variable(tf.truncated_normal(shape=(self.n_hid * self.factor, self.n_hid * self.factor)), dtype=tf.float32, name="W_CS")
            self.tf_W_SS = tf.Variable(tf.truncated_normal(shape=(self.n_hid * self.factor, self.n_hid * self.factor)), dtype=tf.float32, name="W_SS")
            self.tf_b_S  = tf.Variable(tf.zeros([self.n_hid * self.factor]), dtype=tf.float32, name="b_S")

            self.tf_W_So = tf.Variable(tf.truncated_normal(shape=(self.n_hid * self.factor, self.n_out)), dtype=tf.float32, name="W_So")
            self.tf_b_o  = tf.Variable(tf.zeros([self.n_out]), dtype=tf.float32, name="b_o")
            
            self.tf_S_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid * self.factor, dtype=np.float32), 
                                                        shape=(self.n_hid * self.factor), name="S_tm1")
            # ********************************************************
            #                        ... RNN End
            # ********************************************************
            
            
            # ********************************************************
            #                    Attention Start ...
            # ********************************************************
            self.tf_W_alphaS = tf.Variable(tf.truncated_normal(shape=[self.n_hid * self.factor, self.n_hid * self.factor]), dtype=tf.float32, name="W_alphaS")
            self.tf_W_alphaa = tf.Variable(tf.truncated_normal(shape=[self.n_hid * self.factor, self.n_hid * self.factor]), dtype=tf.float32, name="W_alphaa")
            self.tf_b_alpha  = tf.Variable(tf.zeros([self.n_hid * self.factor]), dtype=tf.float32, name="b_alpha")
            
            
            self.tf_W_alphah = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid * self.factor]), dtype=tf.float32, name="W_alphah")
            self.tf_b_alphah  = tf.Variable(tf.zeros([self.n_hid * self.factor]), dtype=tf.float32, name="b_alphah")
            
            self.tf_W_alphao = tf.Variable(tf.truncated_normal(shape=[self.n_hid * self.factor, self.n_hid]), dtype=tf.float32, name="W_alphao")
            self.tf_b_alphao  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_alphao")
            
            self.tf_W_alphav = tf.Variable(tf.truncated_normal(shape=[self.n_hid * self.factor]), dtype=tf.float32, name="W_alphav")
            
            self.tf_e_t = None
            self.tf_C_t = None
            # ********************************************************
            #                     ... Attention End
            # ********************************************************
            
            self.tf_out_t = tf.constant(np.zeros(self.n_out, dtype=np.float32))
            (self.tf_S_t, self.tf_out_t) = tf.scan(self.rnn_cell, self.tf_y, initializer=(self.tf_S_tm1, self.tf_out_t))
            
            self.tf_loss = -tf.reduce_mean(
                self.tf_y * tf.log(self.tf_out_t + self.epsilon) +
                (1. - self.tf_y) * tf.log(1. - self.tf_out_t + self.epsilon)
            )
            
            self.tf_cost = self.tf_loss
            self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.tf_cost)
            self.tf_optimize = self.tf_optimizer
            
            self.tf_config = tf.ConfigProto()
            self.tf_config.gpu_options.allow_growth = True
            self.tf_init = tf.global_variables_initializer()
            
            # Weights & bias histogram
            tf.summary.histogram("W_CS", self.tf_W_CS)
            tf.summary.histogram("W_SS", self.tf_W_SS)
            tf.summary.histogram("b_S", self.tf_b_S)

            tf.summary.histogram("W_alphaS", self.tf_W_alphaS)
            tf.summary.histogram("W_alphaa", self.tf_W_alphaa)
            tf.summary.histogram("b_alpha", self.tf_b_alpha)

            tf.summary.histogram("W_So", self.tf_W_So)
            tf.summary.histogram("b_o", self.tf_b_o)
            
            self.tf_summaries = tf.summary.merge_all()
            
    def _attention_e(self, tmp, h_t):
        """
        h_t         (100,)
        tf_W_alphaa (100, 100)
        tf_S_tm1    (100,)
        tf_W_alphaS (100, 100)
        tf_b_alpha  (100,)
        """
    
        result = tf.tanh(tf.matmul(tf.expand_dims(h_t, 0), self.tf_W_alphaa) + 
                         tf.matmul(tf.expand_dims(self.tf_S_tm1, 0), self.tf_W_alphaS) + 
                         self.tf_b_alpha)
        
        # result = tf.tanh(tf.matmul(result, self.tf_W_alphah) + self.tf_b_alphah)
        # result = tf.tanh(tf.matmul(result, self.tf_W_alphao) + self.tf_b_alphao)
        
        result = tf.multiply(self.tf_W_alphav, result)
                
        return tf.reshape(result, [result.shape[1]])
        
    def _attention_alpha(self, tmp, e_t):
        """
           e_t    (1, 100)
        tf_e_t (None, 100)
        """
    
        result = tf.truediv(tf.expand_dims(tf.exp(e_t), 0), tf.reduce_sum(tf.exp(self.tf_e_t), 0))
        return tf.reshape(result, [result.shape[1]])
    
    @staticmethod
    def _attention_context(alpha, h):
        """
        alpha (None, 100)
        h     (100,)
        """
        return tf.reduce_sum(tf.multiply(alpha, h), 0)
    
    def a_cell(self, _out, x_t):    
        """
        x_t     (7,)
        h_tm1   (100,)
        h_t     (1, 100)
        tf_W_ih (7, 100)
        tf_W_hh (100, 100)
        tf_b_h  (100,)
        """
        
        h_tm1 = _out
        h_t = tf.tanh(tf.matmul(tf.expand_dims(x_t, 0),   self.tf_W_ih) +
                      tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_hh) + 
                      self.tf_b_h)
        # a_t = tf.sigmoid(tf.matmul(h_t, self.tf_W_ha) + self.tf_b_a)

        return tf.reshape(h_t, [h_t.shape[1]]) #, tf.reshape(a_t, [self.n_hid])
    
    def rnn_cell(self, _out, y_t):
        """
        y_t not in use, only needed to create right output sequence length
        
        y_t        (None, 7)
        tf_e_t     (None, 100)
        tf_alpha_t (None, 100)
        tf_C_t     (None, 100)
        
        """
        (S_tm1, _) = _out
        
        
        self.tf_e_t =     tf.scan(self._attention_e, self.tf_h_t, initializer=(tf.constant(np.zeros(self.n_hid * self.factor, dtype=np.float32))))
        self.tf_alpha_t = tf.scan(self._attention_alpha, self.tf_e_t)
        self.tf_C_t =     Attention._attention_context(self.tf_alpha_t, self.tf_h_t)
        
        S_t = tf.tanh(tf.matmul(tf.expand_dims(self.tf_C_t, 0), self.tf_W_CS) +
                      tf.matmul(tf.expand_dims(S_tm1, 0), self.tf_W_SS) + self.tf_b_S)
        out_t = tf.sigmoid(tf.matmul(S_t, self.tf_W_So) + self.tf_b_o)
        
        self.tf_S_tm1 = tf.reshape(S_t, [S_t.shape[1]])

        return self.tf_S_tm1, tf.reshape(out_t, [out_t.shape[1]])
        
        
        
        
        
        
        
class RNN2(AbstractRNN):
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        self.name = "_RNN2"

        # NON GRAPH STUFF
        self.save_file = os.path.join(os.path.dirname(__file__), "../tf_save_rnn2/rnn2.chkp")

        # TF GRAPH
        with self.tf_graph.as_default():
            self.tf_x = tf.placeholder(shape=(None, self.n_in), dtype=tf.float32, name="INPUT")
            self.tf_y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float32, name="LABEL")

            self.tf_W_ih = tf.Variable(tf.truncated_normal(shape=(self.n_in, self.n_hid)), dtype=tf.float32, name="W_ih")
            self.tf_W_hh = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_hid)), dtype=tf.float32, name="W_hh")
            self.tf_b_h  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_h")

            self.tf_W_ha = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_out)), dtype=tf.float32, name="W_ha")
            self.tf_b_a  = tf.Variable(tf.zeros([self.n_out]), dtype=tf.float32, name="b_a")
            
            self.tf_h_tm1 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), 
                                                        shape=(self.n_hid), name="h_tm1")

            self.tf_a = tf.constant(np.zeros(self.n_in, dtype=np.float32))
            
            
            self.tf_W_ah  = tf.Variable(tf.truncated_normal(shape=[self.n_in, self.n_hid]), dtype=tf.float32, name="W_ah")
            self.tf_W_hh2 = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid]), dtype=tf.float32, name="W_hh2")
            self.tf_b_h2  = tf.Variable(tf.zeros([self.n_hid]), dtype=tf.float32, name="b_h2")

            self.tf_W_ho = tf.Variable(tf.truncated_normal(shape=(self.n_hid, self.n_out)), dtype=tf.float32, name="W_ho")
            self.tf_b_o  = tf.Variable(tf.zeros([self.n_out]), dtype=tf.float32, name="b_o")
            
            self.tf_h_tm1_2 = tf.placeholder_with_default(input=np.zeros(self.n_hid, dtype=np.float32), 
                                                          shape=(self.n_hid), name="h_tm1_2")
                                                        
            self.tf_out_t = tf.constant(np.zeros(self.n_out, dtype=np.float32))
            
            (self.tf_h, self.tf_a, self.tf_h2, self.tf_out_t) = tf.scan(self.rnn_cell, self.tf_x, initializer=(self.tf_h_tm1, self.tf_a, self.tf_h_tm1_2, self.tf_out_t))

            self.tf_loss = -tf.reduce_mean(
                self.tf_y * tf.log(self.tf_out_t + self.epsilon) +
                (1. - self.tf_y) * tf.log(1. - self.tf_out_t + self.epsilon)
            )
            
            self.tf_cost = self.tf_loss
            self.tf_optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.tf_cost)
            self.tf_optimize = self.tf_optimizer

            # Weights & bias histogram
            tf.summary.histogram("W_ih", self.tf_W_ih)
            tf.summary.histogram("W_hh", self.tf_W_hh)
            tf.summary.histogram("b_h", self.tf_b_h)

            tf.summary.histogram("W_ho", self.tf_W_ho)
            tf.summary.histogram("b_o", self.tf_b_o)

            self.tf_config = tf.ConfigProto()
            self.tf_config.gpu_options.allow_growth = True
            self.tf_init = tf.global_variables_initializer()
            self.tf_summaries = tf.summary.merge_all()

    def rnn_cell(self, _out, x_t):
        (h_tm1, _, h_tm1_2, _) = _out
        h_t = tf.tanh(tf.matmul(tf.expand_dims(x_t, 0),   self.tf_W_ih) +
                      tf.matmul(tf.expand_dims(h_tm1, 0), self.tf_W_hh) + self.tf_b_h)
        a_t = tf.sigmoid(tf.matmul(h_t, self.tf_W_ha) + self.tf_b_a)
        
        
        h_t2 = tf.tanh(tf.matmul(a_t, self.tf_W_ah) +
                      tf.matmul(tf.expand_dims(h_tm1_2, 0), self.tf_W_hh2) + self.tf_b_h2)
        out_t = tf.sigmoid(tf.matmul(h_t2, self.tf_W_ho) + self.tf_b_o)
        
        
        return tf.reshape(h_t, [self.n_hid]), tf.reshape(a_t, [self.n_in]), tf.reshape(h_t2, [self.n_hid]), tf.reshape(out_t, [self.n_out])
        
    def sample(self, start_sequence, end_sequence, end_count=1, n=1):
        with tf.Session(config=self.tf_config, graph=self.tf_graph) as sess:
            sess.run(self.tf_init)
            saver = tf.train.Saver()
            saver.restore(sess, self.save_file)

            out_samples = []
            indices = np.arange(self.n_out)
            
            for i in range(n):
                h =  np.zeros(self.n_hid, dtype=np.float32)
                h2 = np.zeros(self.n_hid, dtype=np.float32)
                
                out = start_sequence
            
                out_sequences = [out]
                count = 0
                loop = True
                
                while loop:
                    h, h2, out = sess.run([self.tf_h, self.tf_h2, self.tf_out_t], feed_dict={self.tf_x: out, self.tf_h_tm1: h, self.tf_h_tm1_2: h2})                    
                    
                    h = np.reshape(h, self.n_hid)
                    h2 = np.reshape(h2, self.n_hid)

                    p = out[0] / out[0].sum()

                    choice = np.random.choice(indices, p=p)
                    out[0][:] = 0.0
                    out[0][choice] = 1.0

                    out_sequences.append(out[0])

                    if (out == end_sequence).all():
                        count += 1

                    loop = not (count == end_count)

                out_samples.append(np.asarray(out_sequences))

        return np.asarray(out_samples)
        
        
        
        
        
        
        
        
        
        
        
        
        