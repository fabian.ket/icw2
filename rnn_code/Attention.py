import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
from abc import ABC, abstractmethod
import logging
import rnn_code.reberGrammar as rg  # i have to import it that way, so it will work in the notebooks
import os

from IPython.display import clear_output

logging.getLogger('tensorflow').disabled = True


def get_rg_sample_accuracy(model=None, n=100, end_count=1, verbose=1):

    sample_batch = 1000 if n >= 1000 else n

    words = []
    in_grammar = []

    start_sequence = np.asarray([[1., 0., 0., 0., 0., 0., 0.]], dtype=np.float32)
    end_sequence = np.asarray([[0., 0., 0., 0., 0., 0., 1.]], dtype=np.float32)

    if end_count <= 1:
        grammar_tester = rg.in_grammar
    else:
        grammar_tester = rg.in_embedded_grammar

    acc = 0
    _n = n

    start_time = time.time()

    while _n > 0:
        _n -= sample_batch

        if _n < 0:
            sample_batch += _n

        samples = model.sample(start_sequence, end_sequence, n=sample_batch, end_count=end_count)

        for sample in samples:
            word = rg.sequenceToWord(sample)
            words.append(word)
            in_grammar.append(grammar_tester(word))

        acc = sum(in_grammar) / len(in_grammar)

        if verbose == 1:
            print("Sample acc: {}% over {} samples".format(acc * 100, len(in_grammar)))

    if verbose == 1:
        print("Duration: {}sec".format(time.time() - start_time))

    return acc, words


class AbstractModel(ABC):
    """
    Abstract model class
    """
    
    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        
        #*****************************
        #   tensorflow attributes
        #*****************************
        
        self.tf_graph = tf.Graph()
        
        self.tf_sess = None
        self.tf_init = None
        
        self.tf_config = tf.ConfigProto()
        self.tf_config.gpu_options.allow_growth = True
        
        self.tf_saver = None
        
        self.out = None
        
        self.loss = None
        self.optimizer = None
        self.summaries = None
        
        #*****************************
        #    default attributes
        #*****************************
        
        self.suffix = "_ABCModel"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_abcModel/abcModel.chkp")

        self.n_in  = n_in
        self.n_hid = n_hid
        self.n_out = n_out

        self.learning_rate = 0.005
        self.epsilon = 1e-30
        
    def init_sess(self):
        self.tf_sess = tf.Session(config=self.tf_config, graph=self.tf_graph)
        self.tf_sess.run(self.tf_init)
        
    def save(self):
        self.tf_saver.save(self.tf_sess, self.save_path)
    
    def load(self):
        self.tf_saver.restore(self.tf_sess, self.save_path)
        
    def train(self, x_train, y_train, epochs=20, load=False):
        n_examples = x_train.shape[0]
        
        if load:
            self.load()

        writer = tf.summary.FileWriter('logs', self.tf_sess.graph, filename_suffix=self.suffix)
            
        train_cost_list = []

        inputs  = x_train
        outputs = y_train
        
        length = len(inputs)

        t0 = time.time()
        for i in range(epochs):
            if i % 10 == 0:
                clear_output(wait=True)

            train_cost = 0.0

            print("Epoch: {:3} / {:3} | ".format(i+1, epochs), end="")

            j = 0
            for _x, _y in zip(inputs, outputs):
                _x = np.asarray(_x)
                _y = np.asarray(_y)
                
                _, cost, summaries = self.tf_sess.run([self.optimizer, self.loss, self.summaries], 
                                                      feed_dict={self.x: _x, self.y: _y})
                train_cost += cost

                j +=1

                if n_examples >= 10 and j % (n_examples // 10) == 0:
                    print("{}% ".format(j / n_examples * 100), end="")
                
                writer.add_summary(summaries, global_step=(i * length + j))

            train_cost_list.append(train_cost)

            print("| Cost: {} ".format(train_cost))

        print("")
        print("Duration: {}sec".format(time.time() - t0))

        plt.plot(train_cost_list)
        plt.xlabel("Epochs")
        plt.ylabel("Cost")
        plt.show()
        
    def predict(self, x, y, load=False):
        
        if load:
            self.load()
            
        out = self.tf_sess.run([self.out], feed_dict={self.x: x, self.y: y})
        
        return np.asarray(out)
        


class Attention(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_Attention"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_attention/attention.chkp")
        
        pwd = tf.placeholder_with_default
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")


            # encoder weights (Activation)
            self.W_xh  = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xh")
            self.W_hh  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_hh")
            self.b_h   = tf.Variable(tf.zeros([self.n_hid], dtype=tf.float64), name="b_h")
            
            self.h_tm1 = pwd(tf.zeros(self.n_hid, dtype=tf.float64), shape=(self.n_hid), name="a_tm1")
            
            # FIRST scan for a calculations
            self.h = tf.scan(self.encoder_cell, self.x, initializer=(self.h_tm1))
            
            
            # self.s_tm1 = pwd(np.zeros(self.n_hid, dtype=np.float64), shape=(self.n_hid), name="h_tm1")
            
            self.W_hs = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="W_hs")
            self.s_tm1 = tf.reshape(tf.tanh( tf.matmul(tf.expand_dims(self.h[-1], 0), self.W_hs) ), [self.n_hid])
            
            # Attention weights
            self.W_he = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_he")
            self.W_se = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_se")
            self.b_e1 = tf.Variable(tf.zeros([self.n_hid], dtype=tf.float64), name="b_e1")
            
            self.U_ee = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_ee")
            self.b_e2 = tf.Variable(tf.zeros([self.n_hid], dtype=tf.float64), name="b_e2")
            
            self.V_ee = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="V_ee")
            self.b_e3 = tf.Variable(tf.zeros([self.n_hid], dtype=tf.float64), name="b_e3")
            
            self.W_ve = tf.Variable(tf.truncated_normal(shape=[self.n_hid, 1], dtype=tf.float64), name="W_ve")
            
            self.e = None
            self.alpha = None

            
            # Hidden Weights
            self.W_Cs  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_Cs")
            self.W_ss  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_ss")
            self.W_ys  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_ys")
            self.b_s   = tf.Variable(tf.zeros([self.n_hid], dtype=tf.float64), name="b_s")
            

            # Output weights
            self.W_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="W_so")
            self.W_yo = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_out], dtype=tf.float64), name="W_yo")
            self.W_Co = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="W_Co")
            self.b_o = tf.Variable(tf.zeros([self.n_out], dtype=tf.float64), name="b_o")
            
            self.out  = tf.constant(0.0, shape=[self.n_out], dtype=tf.float64, name="out")


            # predictions calculations
            
            # teacher forcing
            zeros = tf.constant(0.0, shape=[1, self.y.shape[1]], dtype=tf.float64)
            y = self.y[0:-1]
            y = tf.concat([zeros, y], axis=0)
            
            _, self.out = tf.scan(self.decoder_cell, y, initializer=(self.s_tm1, self.out))
            
            
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )

#             self.loss = tf.losses.sigmoid_cross_entropy(self.y, self.out)
#             self.loss = tf.losses.softmax_cross_entropy(self.y, self.out)
            
#             self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
#             self.optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate).minimize(self.loss)
#             self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(self.loss)
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
            
#             self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
#             self.gradients, self.variables = zip(*self.optimizer.compute_gradients(self.loss))
#             self.gradients, _ = tf.clip_by_global_norm(self.gradients, 10.0)
#             self.optimizer = self.optimizer.apply_gradients(zip(self.gradients, self.variables))
            
            
            self.tf_init = tf.global_variables_initializer()
            
            save_dict = {
                "W_xh": self.W_xh,
                "W_hh": self.W_hh,
                
                "W_hs": self.W_hs,
                
                "W_he": self.W_he,
                "W_se": self.W_se,
                "W_ve": self.W_ve,
                
                "W_Cs": self.W_Cs,
                "W_ss": self.W_ss,
                "W_ys": self.W_ys,
                
                "W_so": self.W_so,
                "W_yo": self.W_yo,
                "W_Co": self.W_Co,
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            tf.summary.histogram("W_xh", self.W_xh)
            tf.summary.histogram("U_hh", self.W_hh)
            
            self.summaries = tf.summary.merge_all()
            
        self.init_sess()

    def encoder_cell(self, _out, x_t):
        
        mul  = tf.matmul
        tanh = tf.tanh
        
        h_tm1 = _out
        
        x_t   = tf.expand_dims(x_t,   0)
        h_tm1 = tf.expand_dims(h_tm1, 0)
        
        h_t = tanh(mul(x_t, self.W_xh) + mul(h_tm1, self.W_hh) + self.b_h)

        return tf.reshape(h_t, [h_t.shape[1]])

    def attention_e(self):
        
        mul  = tf.matmul
        sig  = tf.sigmoid
        tanh = tf.tanh
        
        s_tm1 = tf.expand_dims(self.s_tm1, 0)
        
        result = tanh( mul(s_tm1, self.W_se) + mul(self.h, self.W_he) + self.b_e1 )
        
        result = tanh( mul(result, self.U_ee) + self.b_e2 )
        result = tanh( mul(result, self.V_ee) + self.b_e3 )
        
        result =  sig( mul(result, self.W_ve) )
        
        return result
    
    def attention_alpha(self):
        exp_e = tf.exp(self.e)
        return tf.truediv(exp_e, tf.reduce_sum(exp_e, 0))

    def attention_context(self):
        return tf.reduce_sum(tf.multiply(self.alpha, self.h), 0)


    def decoder_cell(self, _out, y_t):
        # y_t not in use, only here for output length
        self.s_tm1, out_tm1 = _out
        
        mul  = tf.matmul
        mup  = tf.multiply
        sig  = tf.sigmoid
        tanh = tf.tanh
        
        # Attention e
        self.e = self.attention_e()
        
        # Attention weights
        self.alpha = self.attention_alpha()
        
        # Attention context vector
        C_t = self.attention_context()
        # C_t = y_t
        
        C_t = tf.expand_dims(C_t, 0)
        s_tm1 = tf.expand_dims(self.s_tm1, 0)
        out_tm1 = tf.expand_dims(out_tm1, 0)
        
        y_t = tf.expand_dims(y_t, 0)
        
        #Output
        s_t = tanh( mul(C_t, self.W_Cs) + mul(s_tm1, self.W_ss) + mul(y_t, self.W_ys) + self.b_s )
        
        out = sig( mul(C_t, self.W_Co) + mul(s_t, self.W_so) + mul(y_t, self.W_yo) + self.b_o )
        
        return tf.reshape(s_t, [s_t.shape[1]]), tf.reshape(out, [out.shape[1]])
        

class AttentionDot(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_AttentionDot"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_attentionDot/attentionDot.chkp")
        
        pwd = tf.placeholder_with_default
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")


            # encoder weights (Activation)
            self.W_xh  = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xh")
            self.W_hh  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_hh")
            
            self.h_tm1 = pwd(tf.zeros(self.n_hid, dtype=tf.float64), shape=(self.n_hid), name="a_tm1")
            
            # FIRST scan for a calculations
            self.h = tf.scan(self.encoder_cell, self.x, initializer=(self.h_tm1))
            
            
            # self.s_tm1 = pwd(np.zeros(self.n_hid, dtype=np.float64), shape=(self.n_hid), name="h_tm1")
            
            self.W_hs = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="W_hs")
            self.s_tm1 = tf.reshape(tf.tanh( tf.matmul(tf.expand_dims(self.h[-1], 0), self.W_hs) ), [self.n_hid])
            
            # Attention weights
            self.W_he = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_he")
            self.W_se = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_se")
            self.W_ve = tf.Variable(tf.truncated_normal(shape=[self.n_hid, 1], dtype=tf.float64), name="W_ve")
            
            self.e = None
            self.alpha = None

            
            # Hidden Weights
            self.W_Cs  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_Cs")
            self.W_ss  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_ss")
            self.W_ys  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_ys")
            

            # Output weights
            self.W_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="W_so")
            self.W_yo = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_out], dtype=tf.float64), name="W_yo")
            self.W_Co = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="W_Co")
            self.out  = tf.constant(0.0, shape=[self.n_out], dtype=tf.float64, name="out")


            # predictions calculations
            
            # teacher forcing
            zeros = tf.constant(0.0, shape=[1, self.y.shape[1]], dtype=tf.float64)
            y = self.y[0:-1]
            y = tf.concat([zeros, y], axis=0)
            
            _, self.out = tf.scan(self.decoder_cell, y, initializer=(self.s_tm1, self.out))
            
            
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )
            
#             self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(self.loss)
            
            
            self.tf_init = tf.global_variables_initializer()
            
            save_dict = {
                "W_xh": self.W_xh,
                "W_hh": self.W_hh,
                
                "W_hs": self.W_hs,
                
                "W_Cs": self.W_Cs,
                "W_ss": self.W_ss,
                "W_ys": self.W_ys,
                
                "W_so": self.W_so,
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            tf.summary.histogram("W_xh", self.W_xh)
            tf.summary.histogram("U_hh", self.W_hh)
            
            self.summaries = tf.summary.merge_all()
            
        self.init_sess()

    def encoder_cell(self, _out, x_t):
        
        mul  = tf.matmul
        tanh = tf.tanh
        
        h_tm1 = _out
        
        x_t   = tf.expand_dims(x_t,   0)
        h_tm1 = tf.expand_dims(h_tm1, 0)
        
        h_t = tanh(mul(x_t, self.W_xh) + mul(h_tm1, self.W_hh))

        return tf.reshape(h_t, [h_t.shape[1]])

    def attention_e(self):
        return tf.tensordot(self.h, tf.expand_dims(self.s_tm1, 1), axes=1)
    
    def attention_alpha(self):
        exp_e = tf.exp(self.e)
        return tf.truediv(exp_e, tf.reduce_sum(exp_e, 0))

    def attention_context(self):
        return tf.reduce_mean(tf.multiply(self.alpha, self.h), 0)


    def decoder_cell(self, _out, y_t):
        # y_t not in use, only here for output length
        self.s_tm1, out_tm1 = _out
        
        mul  = tf.matmul
        mup  = tf.multiply
        sig  = tf.sigmoid
        tanh = tf.tanh
        
        # Attention e
        self.e = self.attention_e()
        
        # Attention weights
        self.alpha = self.attention_alpha()
        
        # Attention context vector
        C_t = self.attention_context()
        # C_t = y_t
        
        C_t = tf.expand_dims(C_t, 0)
        s_tm1 = tf.expand_dims(self.s_tm1, 0)
        out_tm1 = tf.expand_dims(out_tm1, 0)
        
        y_t = tf.expand_dims(y_t, 0)
        
        #Output
        s_t = tanh( mul(C_t, self.W_Cs) + mul(s_tm1, self.W_ss) + mul(y_t, self.W_ys) )
        out =  sig( mul(C_t, self.W_Co) + mul(s_t, self.W_so) + mul(y_t, self.W_yo) )
        
        return tf.reshape(s_t, [s_t.shape[1]]), tf.reshape(out, [out.shape[1]])
        

class AttentionGRU(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_AttentionGRU"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_attentionGRU/attentionGRU.chkp")
        
        pwd = tf.placeholder_with_default
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")
            
            # encoder weights
            self.W_xh = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xh")
            self.U_hh = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hh")
            
            self.W_xz = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xz")
            self.U_hz = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hz")
            
            self.W_xr = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xr")
            self.U_hr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hr")
            
            self.h_tm1 = pwd(tf.zeros(self.n_hid, dtype=tf.float64), shape=self.n_hid, name="h_tm1")
            
            # *************************************
            #       calculate encoder states
            # *************************************
            self.h = tf.scan(self.encoder_cell, self.x, initializer=(self.h_tm1))
            
            self.W_hs = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="W_hs")
            self.s_tm1 = tf.reshape(tf.tanh( tf.matmul(tf.expand_dims(self.h[-1], 0), self.W_hs) ), [self.n_hid])
            
            # Attention weights
            self.W_se = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_se")
            self.U_he = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_he")
            self.v_e  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, 1], dtype=tf.float64), name="v_e")
            
            self.e = None
            self.alpha = None
            
            # Hidden Weights
            self.W_ys = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_ys")
            self.U_ss = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_ss")
            self.C_Cs = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cs")
            
            self.W_yz = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_yz")
            self.U_sz = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_sz")
            self.C_Cz = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cz")
            
            self.W_yr = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_yr")
            self.U_sr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_sr")
            self.C_Cr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cr")
            
            # Output weights
            self.W_yo = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_out], dtype=tf.float64), name="W_yo")
            self.U_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="U_so")
            self.C_Co = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="C_Co")
            
            self.out = tf.constant(0.0, shape=[self.n_out], dtype=tf.float64, name="predictions")

            # predictions calculations
            # teacher forcing
            zeros = tf.constant(0.0, shape=[1, self.y.shape[1]], dtype=tf.float64)
            y = self.y[0:-1]
            y = tf.concat([zeros, y], axis=0)

            self.s, self.out = tf.scan(self.decoder_cell, y, initializer=(self.s_tm1, self.out))
            
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )
            
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
            
            self.tf_init = tf.global_variables_initializer()
            
            save_dict = {
                "W_xh": self.W_xh,
                "U_hh": self.U_hh,
            
                "W_xz": self.W_xz,
                "U_hz": self.U_hz,
            
                "W_xr": self.W_xr,
                "U_hr": self.U_hr,
                
                
                "W_hs": self.W_hs,
                
                
                "W_se": self.W_se,
                "U_he": self.U_he,
                "v_e":  self.v_e,
                
                
                "W_ys": self.W_ys,
                "U_ss": self.U_ss,
                "C_Cs": self.C_Cs,
            
                "W_yz": self.W_yz,
                "U_sz": self.U_sz,
                "C_Cz": self.C_Cz,
            
                "W_yr": self.W_yr,
                "U_sr": self.U_sr,
                "C_Cr": self.C_Cr,
                
                
                "W_yo": self.W_yo,
                "U_so": self.U_so,
                "C_Co": self.C_Co
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            # summaries
            tf.summary.histogram("W_xh", self.W_xh)
            self.summaries = tf.summary.merge_all()
            
        self.init_sess()
    
    def encoder_cell(self, _out, x_t):
        
        mup  = tf.multiply
        mul  = tf.matmul
        sig  = tf.sigmoid
        tanh = tf.tanh
        
        h_tm1 = tf.expand_dims(_out, 0)
        x_t   = tf.expand_dims( x_t, 0)
        

        z_t = sig( mul(x_t, self.W_xz) + mul(h_tm1, self.U_hz) )
        r_t = sig( mul(x_t, self.W_xr) + mul(h_tm1, self.U_hr) )
        
        h_tt = tanh( mul(x_t, self.W_xh) + mul(mup(r_t, h_tm1), self.U_hh) )
        
        h_t = mup(1 - z_t, h_tm1) + mup(z_t, h_tt)
        
        return tf.reshape(h_t, [h_t.shape[1]])

    def attention_e(self):
        
        mul  = tf.matmul
        tanh = tf.tanh
        
        s_tm1 = tf.expand_dims(self.s_tm1, 0)
        
        return mul( tanh(mul(s_tm1, self.W_se) + mul(self.h, self.U_he)), self.v_e )
        
        # return mul( tanh(mul(self.h, self.U_he)), self.v_e )

    def attention_alpha(self):
        # exp_e = tf.exp(self.e)
        # return tf.truediv(exp_e, tf.reduce_sum(exp_e, 0))
        
        return tf.nn.softmax(self.e, axis=0)

    def attention_context(self):
        return tf.reduce_sum(tf.multiply(self.alpha, self.h), 0)
        # return tf.reduce_sum(self.h, 0)

    def decoder_cell(self, _out, y_t):
        # y_t not in use, only here for output length
        
        mup  = tf.multiply
        mul  = tf.matmul
        sig  = tf.sigmoid
        tanh = tf.tanh
        
        self.s_tm1, out_tm1 = _out
        
        s_tm1   = tf.expand_dims(self.s_tm1, 0)
        out_tm1 = tf.expand_dims(out_tm1, 0)
        y_t     = tf.expand_dims(y_t, 0)
        
        # Attention e
        self.e = self.attention_e()
        
        # Attention weights
        self.alpha = self.attention_alpha()
        
        # Attention context vector
        C_t = self.attention_context()
        # C_t = y_t
        
        C_t = tf.expand_dims(C_t, 0)
        
        #Output
        z_t  =  sig( mul(y_t, self.W_yz) + mul(s_tm1, self.U_sz) + mul(C_t, self.C_Cz) )
        r_t  =  sig( mul(y_t, self.W_yr) + mul(s_tm1, self.U_sr) + mul(C_t, self.C_Cr) )
        
        s_tt = tanh( mul(y_t, self.W_ys) + mul(mup(r_t, s_tm1), self.U_ss) + mul(C_t, self.C_Cs) )
        s_t  =  mup((1 - z_t), s_tm1) + mup(z_t, s_tt)
        
        # s_t = s_tm1
        
        out = sig( mul(y_t, self.W_yo) + mul(s_t, self.U_so) + mul(C_t, self.C_Co) )
        # out = sig( mul(s_t, self.U_so) )
        # out = sig( mul(C_t, self.C_Co) )
        
        return tf.reshape(s_t, [s_t.shape[1]]), tf.reshape(out, [out.shape[1]])

    def sample(self, start_sequence=None, end_sequence=None, end_count=1, n=1):

        if start_sequence is None:
            start_sequence = np.asarray([[1., 0., 0., 0., 0., 0., 0.]], dtype=np.float64)

        if end_sequence is None:
            end_sequence = np.asarray([[0., 0., 0., 0., 0., 0., 1.]], dtype=np.float64)

        out_samples = []
        indices = np.arange(self.n_out)

        for i in range(n):
            h = np.zeros(self.n_hid, dtype=np.float64)

            x = start_sequence
            y = np.zeros([1, self.n_out], dtype=np.float64)

            out_sequences = [x[0]]
            count = 0
            loop = True

            while loop:
                h, out = self.tf_sess.run([self.h, self.out],
                                          feed_dict={
                                              self.x: x,
                                              self.y: y,
                                              self.h_tm1: h})

                h = np.reshape(h[-1], self.n_hid)

                y = out

                p = out[0] / out[0].sum()

                choice = np.random.choice(indices, p=p)
                out[0][:] = 0.0
                out[0][choice] = 1.0

                out_sequences.append(out[0])

                x = np.asarray(out_sequences)

                if (out == end_sequence).all():
                    count += 1

                loop = not (count == end_count)

            out_samples.append(np.asarray(out_sequences))

        return np.asarray(out_samples)
        

class AttentionGRUDot(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_AttentionGRUDot"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_attentionGRUDot/attentionGRUDot.chkp")
        
        pwd = tf.placeholder_with_default
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")
            
            # encoder weights
            self.W_xh = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xh")
            self.U_hh = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hh")
            
            self.W_xz = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xz")
            self.U_hz = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hz")
            
            self.W_xr = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xr")
            self.U_hr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hr")
            
            self.h_tm1 = pwd(tf.zeros(self.n_hid, dtype=tf.float64), shape=(self.n_hid), name="h_tm1")
            
            # *************************************
            #       calculate encoder states
            # *************************************
            self.h = tf.scan(self.encoder_cell, self.x, initializer=(self.h_tm1))
            
            
            self.W_hs = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="W_hs")
            self.s_tm1 = tf.reshape(tf.tanh( tf.matmul(tf.expand_dims(self.h[-1], 0), self.W_hs) ), [self.n_hid])
            
            
            # Attention weights
            self.W_se = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_se")
            self.U_he = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_he")
            self.v_e  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, 1], dtype=tf.float64), name="v_e")
            
            self.e = None
            self.alpha = None

            
            # Hidden Weights
            self.W_ys  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_ys")    
            self.U_ss  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_ss")
            self.C_Cs  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cs")
            
            self.W_yz  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_yz")    
            self.U_sz  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_sz")
            self.C_Cz  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cz")
            
            self.W_yr  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_yr")    
            self.U_sr  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_sr")
            self.C_Cr  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cr")    
            
            
            # Output weights
            self.W_yo = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_out], dtype=tf.float64), name="W_yo")
            self.U_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="U_so")
            self.C_Co = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="C_Co")
            
            self.out  = tf.constant(0.0, shape=[self.n_out], dtype=tf.float64, name="predictions")


            # predictions calculations
            
            # teacher forcing
            zeros = tf.constant(0.0, shape=[1, self.y.shape[1]], dtype=tf.float64)
            y = self.y[0:-1]
            y = tf.concat([zeros, y], axis=0)
            
            
            _, self.out = tf.scan(self.decoder_cell, y, initializer=(self.s_tm1, self.out))
            
            
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )
            
            
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
            
            
            self.tf_init = tf.global_variables_initializer()
            
            save_dict = {
                "W_xh": self.W_xh,
                "U_hh": self.U_hh,
            
                "W_xz": self.W_xz,
                "U_hz": self.U_hz,
            
                "W_xr": self.W_xr,
                "U_hr": self.U_hr,
                
                
                "W_hs": self.W_hs,
                
                
                "W_se": self.W_se,
                "U_he": self.U_he,
                "v_e":  self.v_e,
                
                
                "W_ys": self.W_ys,
                "U_ss": self.U_ss,
                "C_Cs": self.C_Cs,
            
                "W_yz": self.W_yz,
                "U_sz": self.U_sz,
                "C_Cz": self.C_Cz,
            
                "W_yr": self.W_yr,
                "U_sr": self.U_sr,
                "C_Cr": self.C_Cr,
                
                
                "W_so": self.W_yo,
                "W_so": self.U_so,
                "W_so": self.C_Co
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            # summaries
            tf.summary.histogram("W_xh", self.W_xh)
            self.summaries = tf.summary.merge_all()
            
        self.init_sess()
    
    def encoder_cell(self, _out, x_t):
        
        mup   = tf.multiply
        mul   = tf.matmul
        sig   = tf.sigmoid
        tanh  = tf.tanh
        
        h_tm1 = tf.expand_dims(_out, 0)
        x_t   = tf.expand_dims( x_t, 0)
        
        
        z_t = sig( mul(x_t, self.W_xz) + mul(h_tm1, self.U_hz) )
        r_t = sig( mul(x_t, self.W_xr) + mul(h_tm1, self.U_hr) )
        
        h_tt = tanh( mul(x_t, self.W_xh) + mul(mup(r_t, h_tm1), self.U_hh) )
        
        h_t = mup(1 - z_t, h_tm1) + mup(z_t, h_tt)
        
        return tf.reshape(h_t, [h_t.shape[1]])
    

    def attention_e(self):
        return tf.tensordot(self.h, tf.expand_dims(self.s_tm1, 1), axes=1)
    
    
    def attention_alpha(self):
        # exp_e = tf.exp(self.e)
        # return tf.truediv(exp_e, tf.reduce_sum(exp_e, 0))
        
        return tf.nn.softmax(self.e, axis=0)

    def attention_context(self):
        return tf.reduce_sum(tf.multiply(self.alpha, self.h), 0)


    def decoder_cell(self, _out, y_t):
        # y_t not in use, only here for output length
        
        mup   = tf.multiply
        mul   = tf.matmul
        sig   = tf.sigmoid
        tanh  = tf.tanh
        
        self.s_tm1, out_tm1 = _out
        
        s_tm1   = tf.expand_dims(self.s_tm1, 0)
        out_tm1 = tf.expand_dims(out_tm1, 0)
        y_t     = tf.expand_dims(y_t, 0)
        
        # Attention e
        self.e = self.attention_e()
        
        # Attention weights
        self.alpha = self.attention_alpha()
        
        # Attention context vector
        C_t = self.attention_context()
        # C_t = y_t
        
        C_t = tf.expand_dims(C_t, 0)
        
        #Output
        z_t  =  sig( mul(y_t, self.W_yz) + mul(s_tm1, self.U_sz) + mul(C_t, self.C_Cz) )
        r_t  =  sig( mul(y_t, self.W_yr) + mul(s_tm1, self.U_sr) + mul(C_t, self.C_Cr) )
        s_tt = tanh( mul(y_t, self.W_ys) + mul(mup(r_t, s_tm1), self.U_ss) + mul(C_t, self.C_Cs) )
        
        s_t  =  mup((1 - z_t), s_tm1) + mup(z_t, s_tt)
        # out  =  sig( mul(y_t, self.W_yo) + mul(s_t, self.U_so) + mul(C_t, self.C_Co) ) 
        out  =  sig( mul(s_t, self.U_so) )
        
        return tf.reshape(s_t, [s_t.shape[1]]), tf.reshape(out, [out.shape[1]])
        
        
class AttentionPaperBi(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_AttentionPaperBi"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_attentionPaperBi/attentionPaperBi.chkp")
        
        pwd = tf.placeholder_with_default
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")
            
            # encoder weights
            self.W_xhr = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xhr")
            self.U_hhr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hhr")
            
            self.W_xzr = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xzr")
            self.U_hzr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hzr")
            
            self.W_xrr = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xrr")
            self.U_hrr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hrr")
            
            self.h_tm1r = pwd(tf.zeros(self.n_hid, dtype=tf.float64), shape=(self.n_hid), name="h_tm1r")
            
            
            self.W_xhl = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xhl")
            self.U_hhl = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hhl")
            
            self.W_xzl = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xzl")
            self.U_hzl = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hzl")
            
            self.W_xrl = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xrl")
            self.U_hrl = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hrl")
            
            self.h_tm1l = pwd(tf.zeros(self.n_hid, dtype=tf.float64), shape=(self.n_hid), name="h_tm1l")
            
            # *************************************
            #       calculate encoder states
            # *************************************
            self.hr = tf.scan(self.encoderr_cell, self.x, initializer=(self.h_tm1r))
            self.hl = tf.scan(self.encoderl_cell, self.x, initializer=(self.h_tm1l), reverse=True)
            self.h  = tf.concat([self.hr, self.hl], 1)
            
            # Attention weights
            self.W_se = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_se")
            self.U_he = tf.Variable(tf.truncated_normal(shape=[2 * self.n_hid, self.n_hid], dtype=tf.float64), name="U_he")
            self.v_e  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, 1], dtype=tf.float64), name="v_e")
            
            self.s_tm1 = pwd(np.zeros(self.n_hid, dtype=np.float64), shape=(self.n_hid), name="s_tm1")
            # self.s_tm1 = self.h[-1] 
            
            self.e = None
            self.alpha = None

            
            # Hidden Weights
            self.W_ys  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_ys")    
            self.U_ss  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_ss")
            self.C_Cs  = tf.Variable(tf.truncated_normal(shape=[2 * self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cs")
            
            self.W_yz  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_yz")    
            self.U_sz  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_sz")
            self.C_Cz  = tf.Variable(tf.truncated_normal(shape=[2 * self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cz")
            
            self.W_yr  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_yr")    
            self.U_sr  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_sr")
            self.C_Cr  = tf.Variable(tf.truncated_normal(shape=[2 * self.n_hid, self.n_hid], dtype=tf.float64), name="C_Cr")    
            
            
            # Output weights
            self.W_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="W_so")
            self.out  = tf.constant(0.0, shape=[self.n_out], dtype=tf.float64, name="predictions")


            # predictions calculations
            _, self.out = tf.scan(self.decoder_cell, self.y, initializer=(self.s_tm1, self.out))
            
            
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )
            
            
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
            
#             self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
#             self.gradients, self.variables = zip(*self.optimizer.compute_gradients(self.loss))
#             self.gradients, _ = tf.clip_by_global_norm(self.gradients, 10.0)
#             self.optimizer = self.optimizer.apply_gradients(zip(self.gradients, self.variables))
            
            
            self.tf_init = tf.global_variables_initializer()
            
            save_dict = {
                "W_xhr": self.W_xhr,
                "U_hhr": self.U_hhr,
            
                "W_xzr": self.W_xzr,
                "U_hzr": self.U_hzr,
            
                "W_xrr": self.W_xrr,
                "U_hrr": self.U_hrr,
                
                "W_xhl": self.W_xhl,
                "U_hhl": self.U_hhl,
            
                "W_xzl": self.W_xzl,
                "U_hzl": self.U_hzl,
            
                "W_xrl": self.W_xrl,
                "U_hrl": self.U_hrl,
                
                
                "W_se": self.W_se,
                "U_he": self.U_he,
                "v_e":  self.v_e,
                
                
                "W_ys": self.W_ys,
                "U_ss": self.U_ss,
                "C_Cs": self.C_Cs,
            
                "W_yz": self.W_yz,
                "U_sz": self.U_sz,
                "C_Cz": self.C_Cz,
            
                "W_yr": self.W_yr,
                "U_sr": self.U_sr,
                "C_Cr": self.C_Cr,
                
                
                "W_so": self.W_so
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
        self.init_sess()
    
    def encoderr_cell(self, _out, x_t):
        
        mup   = tf.multiply
        mul   = tf.matmul
        sig   = tf.sigmoid
        tanh  = tf.tanh
        
        h_tm1 = tf.expand_dims(_out, 0)
        x_t   = tf.expand_dims( x_t, 0)
        
        
        z_t = sig( mul(x_t, self.W_xzr) + mul(h_tm1, self.U_hzr) )
        r_t = sig( mul(x_t, self.W_xrr) + mul(h_tm1, self.U_hrr) )
        
        h_tt = tanh( mul(x_t, self.W_xhr) + mul(mup(r_t, h_tm1), self.U_hhr) )
        
        h_t = mup(1 - z_t, h_tm1) + mup(z_t, h_tt)
        
        return tf.reshape(h_t, [h_t.shape[1]])
    
    def encoderl_cell(self, _out, x_t):
        
        mup   = tf.multiply
        mul   = tf.matmul
        sig   = tf.sigmoid
        tanh  = tf.tanh
        
        h_tm1 = tf.expand_dims(_out, 0)
        x_t   = tf.expand_dims( x_t, 0)
        
        
        z_t = sig( mul(x_t, self.W_xzl) + mul(h_tm1, self.U_hzl) )
        r_t = sig( mul(x_t, self.W_xrl) + mul(h_tm1, self.U_hrl) )
        
        h_tt = tanh( mul(x_t, self.W_xhl) + mul(mup(r_t, h_tm1), self.U_hhl) )
        
        h_t = mup(1 - z_t, h_tm1) + mup(z_t, h_tt)
        
        return tf.reshape(h_t, [h_t.shape[1]])
    

    def attention_e(self):
        
        mul   = tf.matmul
        tanh  = tf.tanh
        
        s_tm1 = tf.expand_dims(self.s_tm1, 0)
        
        return mul( tanh(mul(s_tm1, self.W_se) + mul(self.h, self.U_he)), self.v_e )
    
    
    def attention_alpha(self):
        exp_e = tf.exp(self.e)
        return tf.truediv(exp_e, tf.reduce_sum(exp_e, 0))

    def attention_context(self):
        return tf.reduce_sum(tf.multiply(self.alpha, self.h), 0)


    def decoder_cell(self, _out, y_t):
        # y_t not in use, only here for output length
        
        mup   = tf.multiply
        mul   = tf.matmul
        sig   = tf.sigmoid
        tanh  = tf.tanh
        
        self.s_tm1, out_tm1 = _out
        
        s_tm1   = tf.expand_dims(self.s_tm1, 0)
        out_tm1 = tf.expand_dims(out_tm1, 0)
        
        
        # Attention e
        self.e = self.attention_e()
        
        # Attention weights
        self.alpha = self.attention_alpha()
        
        # Attention context vector
        C_t = self.attention_context()
        # C_t = y_t
        
        C_t = tf.expand_dims(C_t, 0)
        
        #Output
        z_t  = sig( mul(out_tm1, self.W_yz) + mul(s_tm1, self.U_sz) + mul(C_t, self.C_Cz) )
        r_t  = sig( mul(out_tm1, self.W_yr) + mul(s_tm1, self.U_sr) + mul(C_t, self.C_Cr) )
        s_tt = tanh( mul(out_tm1, self.W_ys) + mul(mup(r_t, s_tm1), self.U_ss) + mul(C_t, self.C_Cs) )
        
#         z_t  = sig( mul(s_tm1, self.U_sz) + mul(C_t, self.C_Cz) )
#         r_t  = sig( mul(s_tm1, self.U_sr) + mul(C_t, self.C_Cr) )
#         s_tt = tanh( mul(mup(r_t, s_tm1), self.U_ss) + mul(C_t, self.C_Cs) )
        
        s_t  = mup((1 - z_t), s_tm1) + mup(z_t, s_tt)
        out  = sig( mul(s_t, self.W_so) ) 
        
        return tf.reshape(s_t, [s_t.shape[1]]), tf.reshape(out, [out.shape[1]])
        
        
class EnDecoder(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_EnDecoder"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_enDecoder/enDecoder.chkp")
        
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")


            # encoder weights (Activation)
            self.W_xh  = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xh")
            self.W_hh  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_hh")
            
            self.h_tm1 = tf.placeholder_with_default(tf.zeros(self.n_hid, dtype=tf.float64), shape=(self.n_hid), name="h_tm1")
            
            # FIRST scan for a calculations
            self.h = tf.scan(self.encoder_cell, self.x, initializer=(self.h_tm1))
            
            self.s_tm1 = self.h[-1] 
                     # = tf.Variable(tf.zeros(shape=self.n_hid, dtype=tf.float64), name="s_tm1")
                     # = self.h[-1]

            
            # Hidden Weights
            self.W_ys  = tf.Variable(tf.truncated_normal(shape=[self.n_out, self.n_hid], dtype=tf.float64), name="W_ys")
            self.W_ss  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_ss")
            self.W_hs  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="W_hs")
            

            # Output weights
            self.W_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="W_so")
            self.out  = tf.constant(0.0, shape=[self.n_out], dtype=tf.float64, name="predictions")


            # predictions calculations
            _, self.out = tf.scan(self.decoder_cell, self.y, initializer=(self.s_tm1, self.out))
            
            
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )
            
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
            
            
            self.tf_init = tf.global_variables_initializer()
            
            save_dict = {
                "W_xh": self.W_xh,
                "W_hh": self.W_hh,

                "W_ys": self.W_ys,
                "W_ss": self.W_ss,
                
                "W_so": self.W_so,
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            
            tf.summary.histogram("W_xh", self.W_xh)
            
            self.summaries = tf.summary.merge_all()
            
        self.init_sess()

    def encoder_cell(self, _out, x_t):
        
        mul  = tf.matmul
        tanh = tf.tanh
        
        h_tm1 = _out
        
        x_t   = tf.expand_dims(x_t, 0)
        h_tm1 = tf.expand_dims(h_tm1, 0)
        
        h_t = tanh(mul(x_t, self.W_xh) + mul(h_tm1, self.W_hh))

        return tf.reshape(h_t, [h_t.shape[1]])


    def decoder_cell(self, _out, y_t):
        # y_t not in use, only here for output length
        
        mul  = tf.matmul
        sig  = tf.sigmoid
        tanh = tf.tanh
        
        s_tm1, out_tm1 = _out
        
        out_tm1 = tf.expand_dims(out_tm1, 0)
        s_tm1   = tf.expand_dims(s_tm1, 0)
        # h_t     = tf.expand_dims(self.h[-1], 0)

        s_t = tanh(mul(out_tm1, self.W_xh) + mul(s_tm1, self.W_hh))
        out = tf.sigmoid(tf.matmul(s_t, self.W_so)) 
        
        return tf.reshape(s_t, [s_t.shape[1]]), tf.reshape(out, [out.shape[1]])
        
        
class GRU(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_GRU"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_GRU/GRU.chkp")
        
        pwd = tf.placeholder_with_default
        self.dtype = tf.float64
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=self.dtype, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=self.dtype, name="LABEL")
            
            # encoder weights
            self.W_xh = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=self.dtype), name="W_xh")
            self.U_hh = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=self.dtype), name="U_hh")
            
            self.W_xz = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=self.dtype), name="W_xz")
            self.U_hz = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=self.dtype), name="U_hz")
            
            self.W_xr = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=self.dtype), name="W_xr")
            self.U_hr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=self.dtype), name="U_hr")
            
            self.W_ho = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=self.dtype), name="W_ho")
            
            self.h_tm1 = pwd(tf.zeros(self.n_hid, dtype=self.dtype), shape=(self.n_hid), name="h_tm1")
            
            # *************************************
            #       calculate encoder states
            # *************************************
            out_zeros = tf.constant(0.0, shape=[self.n_out], dtype=self.dtype)
            self.h, self.out = tf.scan(self.gru_cell, self.x, initializer=(self.h_tm1, out_zeros))
            
            
            # loss f()
            # self.loss = -tf.reduce_mean(
            #     self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            # )
            
            self.loss = tf.losses.sigmoid_cross_entropy(self.y, self.out)
            
            # optimizer
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(self.loss)
            # self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
            
            # self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=self.learning_rate)
            # self.gradients, self.variables = zip(*self.optimizer.compute_gradients(self.loss))
            # self.gradients, _ = tf.clip_by_global_norm(self.gradients, 10.0)
            # self.optimizer = self.optimizer.apply_gradients(zip(self.gradients, self.variables))
            
            
            # var init
            self.tf_init = tf.global_variables_initializer()
            
            # saver
            save_dict = {
                "W_xh": self.W_xh,
                "U_hh": self.U_hh,
            
                "W_xz": self.W_xz,
                "U_hz": self.U_hz,
            
                "W_xr": self.W_xr,
                "U_hr": self.U_hr,
                
                
                "W_ho": self.W_ho
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            # weights histogram
            # for index, grad in enumerate(self.gradients):
            #     tf.summary.histogram("{}-grad".format(self.gradients[index][1].name), self.gradients[index])
            
            tf.summary.histogram("W_xh", self.W_xh)
            tf.summary.histogram("U_hh", self.U_hh)
            
            tf.summary.histogram("W_xz", self.W_xz)
            tf.summary.histogram("U_hz", self.U_hz)
            
            tf.summary.histogram("W_xr", self.W_xr)
            tf.summary.histogram("U_hr", self.U_hr)
                
            tf.summary.histogram("W_ho", self.W_ho)
            
            self.summaries = tf.summary.merge_all()
            
            
        self.init_sess()
    
    def gru_cell(self, _out, x_t):
        
        mup   = tf.multiply
        mul   = tf.matmul
        sig   = tf.sigmoid
        tanh  = tf.tanh
        
        h_tm1, _  = _out
        
        h_tm1 = tf.expand_dims(h_tm1, 0)
        x_t   = tf.expand_dims( x_t, 0)
        
        
        z_t = sig( mul(x_t, self.W_xz) + mul(h_tm1, self.U_hz) )
        r_t = sig( mul(x_t, self.W_xr) + mul(h_tm1, self.U_hr) )
        
        h_tt = tanh( mul(x_t, self.W_xh) + mul(mup(r_t, h_tm1), self.U_hh) )
        
        h_t = mup(1 - z_t, h_tm1) + mup(z_t, h_tt)
        
        out = sig( mul(h_t, self.W_ho) )
        
        return tf.reshape(h_t, [h_t.shape[1]]), tf.reshape(out, [out.shape[1]])
    
    
class EnDecoderGRU(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_EnDecoderGRU"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_enDecoderGRU/enDecoderGRU.chkp")
        
        pwd = tf.placeholder_with_default
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")
            
            # encoder weights
            self.W_xh = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xh")
            self.U_hh = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hh")
            
            self.W_xz = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xz")
            self.U_hz = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hz")
            
            self.W_xr = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xr")
            self.U_hr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_hr")
            
            self.h_tm1 = pwd(tf.zeros(self.n_hid, dtype=tf.float64), shape=(self.n_hid), name="h_tm1")
            
            # *************************************
            #       calculate encoder states
            # *************************************
            self.h = tf.scan(self.encoder_cell, self.x, initializer=(self.h_tm1))
            
            
            # decoder weights
            self.W_ys = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_ys")
            self.U_ss = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_ss")
            self.V_hs = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="V_hs")
            
            self.W_yz = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_yz")
            self.U_sz = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_sz")
            self.V_hz = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="V_hz")
            
            self.W_yr = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_yr")
            self.U_sr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="U_sr")
            self.V_hr = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_hid], dtype=tf.float64), name="V_hr")
            
            
            self.s_tm1 = pwd(tf.zeros(self.n_hid, dtype=tf.float64), shape=(self.n_hid), name="s_tm1")
            # self.s_tm1 = self.h[-1]
            
            
            # Output weights
            self.W_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid, self.n_out], dtype=tf.float64), name="W_so")
            self.out  = tf.constant(0.0, shape=[self.n_out], dtype=tf.float64, name="predictions")


            # predictions calculations
            
            # teacher forcing
            zeros = tf.constant(0.0, shape=[1, self.y.shape[1]], dtype=tf.float64)
            y = self.y[0:-1]
            y = tf.concat([zeros, y], axis=0)
            
            self.s, self.out = tf.scan(self.decoder_cell, y, initializer=(self.s_tm1, self.out))
            
            
            # loss f()
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )
            

            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss)
            
            
            # var init
            self.tf_init = tf.global_variables_initializer()
            
            # saver
            save_dict = {
                "W_xh": self.W_xh,
                "U_hh": self.U_hh,
            
                "W_xz": self.W_xz,
                "U_hz": self.U_hz,
            
                "W_xr": self.W_xr,
                "U_hr": self.U_hr,
                
                
                "W_ys": self.W_ys,
                "U_ss": self.U_ss,
                "V_hs": self.V_hs,
            
                "W_yz": self.W_yz,
                "U_sz": self.U_sz,
                "V_hz": self.V_hz,
            
                "W_yr": self.W_yr,
                "U_sr": self.U_sr,
                "V_hr": self.V_hr,
                
                
                "W_so": self.W_so
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            # weights histogram
            tf.summary.histogram("W_xh", self.W_xh)
            tf.summary.histogram("U_hh", self.U_hh)
            
            tf.summary.histogram("W_xz", self.W_xz)
            tf.summary.histogram("U_hz", self.U_hz)
            
            tf.summary.histogram("W_xr", self.W_xr)
            tf.summary.histogram("U_hr", self.U_hr)
                
                
            tf.summary.histogram("W_ys", self.W_ys)
            tf.summary.histogram("U_ss", self.U_ss)
            
            tf.summary.histogram("W_yz", self.W_yz)
            tf.summary.histogram("U_sz", self.U_sz)
            
            tf.summary.histogram("W_yr", self.W_yr)
            tf.summary.histogram("U_sr", self.U_sr)
                
            tf.summary.histogram("W_so", self.W_so)
            
            self.summaries = tf.summary.merge_all()
            
            
        self.init_sess()
    
    def encoder_cell(self, _out, x_t):
        
        mup   = tf.multiply
        mul   = tf.matmul
        sig   = tf.sigmoid
        tanh  = tf.tanh
        
        h_tm1 = tf.expand_dims(_out, 0)
        x_t   = tf.expand_dims( x_t, 0)
        
        
        z_t = sig( mul(x_t, self.W_xz) + mul(h_tm1, self.U_hz) )
        r_t = sig( mul(x_t, self.W_xr) + mul(h_tm1, self.U_hr) )
        
        h_tt = tanh( mul(x_t, self.W_xh) + mul(mup(r_t, h_tm1), self.U_hh) )
        
        h_t = mup(1 - z_t, h_tm1) + mup(z_t, h_tt)
        
        return tf.reshape(h_t, [h_t.shape[1]])
    

    def decoder_cell(self, _out, y_t):
        
        mup   = tf.multiply
        mul   = tf.matmul
        sig   = tf.sigmoid
        tanh  = tf.tanh
        
        s_tm1, out_tm1 = _out
        
        out_tm1 = tf.expand_dims(out_tm1, 0)
        s_tm1   = tf.expand_dims(s_tm1, 0)
        h       = tf.expand_dims(self.h[-1], 0)
        
        y_t = tf.expand_dims(y_t, 0)
        
        z_t = sig( mul(y_t, self.W_yz) + mul(s_tm1, self.U_sz) + mul(h, self.V_hz) )
        r_t = sig( mul(y_t, self.W_yr) + mul(s_tm1, self.U_sr) + mul(h, self.V_hr) )
        
        s_tt = tanh( mul(y_t, self.W_ys) + mul(mup(r_t, s_tm1), self.U_ss) + mul(h, self.V_hs) )
        
        s_t = mup(1 - z_t, s_tm1) + mup(z_t, s_tt)
        
        out = sig( mul(s_t, self.W_so) )
        
        return tf.reshape(s_t, [s_t.shape[1]]), tf.reshape(out, [out.shape[1]])
        

    def sample(self, start_sequence=None, end_sequence=None, end_count=1, n=1):
        
        if start_sequence is None:
            start_sequence = np.asarray([[1., 0., 0., 0., 0., 0., 0.]], dtype=np.float64)
            
        if end_sequence is None:
            end_sequence = np.asarray([[0., 0., 0., 0., 0., 0., 1.]], dtype=np.float64)
        

        out_samples = []
        indices = np.arange(self.n_out)
        
        for i in range(n):
            h = np.zeros(self.n_hid, dtype=np.float64)
            s = np.zeros(self.n_hid, dtype=np.float64)
            
            x = start_sequence
            y = [np.zeros([self.n_out], dtype=np.float64)]
        
            out_sequences = [x[0]]
            count = 0
            loop = True
            
            while loop:
                h, s, out = self.tf_sess.run([self.h, self.s, self.out], 
                                             feed_dict={
                                               self.x: x, 
                                               self.y: np.asarray(y), 
                                               self.h_tm1: h,
                                               self.s_tm1: s})
                                               
                
                h = np.reshape(h[-1], self.n_hid)
                s = np.reshape(s[-1], self.n_hid)
                
                
                
                p = out[-1] / out[-1].sum()
                choice = np.random.choice(indices, p=p)
                
                
                
                _y = np.copy(out[-1])
                
                max1 = np.argmax(_y)
                _y[max1] = 0
                
                max2 = np.argmax(_y)
                
                _y[:] = 0
                _y[max1] = 1
                _y[max2] = 1
                
                y.append(_y)
                
                
                out[-1][:] = 0.0
                out[-1][choice] = 1.0
                
                out_sequences.append(out[-1])
                
                x = np.asarray(out_sequences)
                
                if (out[-1] == end_sequence).all():
                    count += 1

                loop = not (count == end_count)

            out_samples.append(np.asarray(out_sequences))

        return np.asarray(out_samples)
        
        
class AttentionIsAllYouNeed(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_AttentionIsAllYouNeed"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_attentionIsAllYouNeed/attentionIsAllYouNeed.chkp")
        
        pwd = tf.placeholder_with_default
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")
            
            
            # Encoder self attention weights
            self.W_xq = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xq")
            self.W_xk = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xk")
            self.W_xv = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xv")
            
            self.W_hh = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="W_hh")
            
            
            # Decoder self attention weights
            self.W_yq = tf.Variable(tf.truncated_normal(shape=[self.n_out,  self.n_hid], dtype=tf.float64), name="W_yq")
            self.W_yk = tf.Variable(tf.truncated_normal(shape=[self.n_out,  self.n_hid], dtype=tf.float64), name="W_yk")
            self.W_yv = tf.Variable(tf.truncated_normal(shape=[self.n_out,  self.n_hid], dtype=tf.float64), name="W_yv")
            
            self.W_ss = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="W_ss")
            self.W_hs = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="W_hs")
            
            self.W_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_out], dtype=tf.float64), name="W_so")
            self.W_ho = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_out], dtype=tf.float64), name="W_ho")
            
            
            
            self.queries = None
            self.keys    = None
            self.values  = None
            

            self.h = self.f_encoder()
            
            
            # teacher forcing
            # zeros = tf.constant(0.0, shape=[1, self.y.shape[1]], dtype=tf.float64)
            # y = self.y[0:-1]
            # y = tf.concat([zeros, y], axis=0)
            
            
            self.s = self.f_decoder()
            
            self.out = self.s
            
            # loss f()
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )
            
            # self.loss = tf.losses.sigmoid_cross_entropy(self.y, self.out)
            
            # optimizer
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(self.loss)
            
            
            # var init
            self.tf_init = tf.global_variables_initializer()
            
            # saver
            save_dict = {
                "W_xq": self.W_xq,
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            # weights histogram
            tf.summary.histogram("W_xq", self.W_xq)
            self.summaries = tf.summary.merge_all()
            
            
        self.init_sess()
    
    
    
    def f_self_attention(self, _out, query_t):
        """
        needed to be done in one methode, because
        reshape with shape ? is not possible
        
        IMPORTANT
        self.queries, self.keys, self.values needs to be set
        before you call this method
        
        """
        
        query_t = tf.reshape(query_t, [query_t.shape[0], 1])
        
        # self attention score ...
        score_t = tf.matmul(self.keys, query_t)
        shape_sqrt = tf.sqrt(tf.cast(query_t.shape[0], dtype=tf.float64))
        score_t = tf.truediv(score_t, shape_sqrt) # only here to get more stable gradients
        
        # softmax
        score_t = tf.exp(score_t)
        score_t = tf.truediv(score_t, tf.reduce_sum(score_t))
        # ... self attention score
        
        # self attention value ...
        softmax_value_t = tf.multiply(self.values, score_t)
        sum_value_t = tf.reduce_sum(softmax_value_t, axis=0)
        
        return sum_value_t
    
    def f_encoder(self):
        
        self.queries = tf.matmul(self.x, self.W_xq)
        self.keys    = tf.matmul(self.x, self.W_xk)
        self.values  = tf.matmul(self.x, self.W_xv)
        
        h = tf.scan(self.f_self_attention, self.queries)
        
        out = h # tf.tanh( tf.matmul(h, self.W_hh) )
        
        return out
    
    def f_decoder(self):
        
        # teacher forcing
        zeros = tf.constant(0.0, shape=[1, self.y.shape[1]], dtype=tf.float64)
        y = self.y[0:-1]
        y = tf.concat([zeros, y], axis=0)
        
        self.queries = tf.matmul(y, self.W_yq)
        self.keys    = tf.matmul(y, self.W_yk)
        self.values  = tf.matmul(y, self.W_yv)
        
        s = tf.scan(self.f_self_attention, self.queries)
        s = tf.sigmoid( tf.matmul(s, self.W_so) + tf.matmul(self.h, self.W_ho) )
        
        return s # tf.sigmoid( tf.matmul(s, self.W_so) )

    
class AttentionIsAllYouNeedWithAttetnion(AbstractModel):

    def __init__(self, n_in=7, n_hid=10, n_out=7, learning_rate=0.005, epsilon=1e-30):
        super().__init__(n_in, n_hid, n_out, learning_rate, epsilon)
        
        self.suffix = "_AttentionIsAllYouNeedWithAttetnion"
        self.save_path = os.path.join(os.path.dirname(__file__), "../tf_save_attentionIsAllYouNeedWithAttetnion/attentionIsAllYouNeedWithAttetnion.chkp")
        
        pwd = tf.placeholder_with_default
        
        with self.tf_graph.as_default():
            self.x = tf.placeholder(shape=(None, self.n_in),  dtype=tf.float64, name="INPUT")
            self.y = tf.placeholder(shape=(None, self.n_out), dtype=tf.float64, name="LABEL")
            
            
            # Encoder self attention weights
            self.W_xq = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xq")
            self.W_xk = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xk")
            self.W_xv = tf.Variable(tf.truncated_normal(shape=[self.n_in,  self.n_hid], dtype=tf.float64), name="W_xv")
            
            self.U_hh = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="U_hh")
            
            
            # Decoder self attention weights
            self.W_yq = tf.Variable(tf.truncated_normal(shape=[self.n_out,  self.n_hid], dtype=tf.float64), name="W_yq")
            self.W_yk = tf.Variable(tf.truncated_normal(shape=[self.n_out,  self.n_hid], dtype=tf.float64), name="W_yk")
            self.W_yv = tf.Variable(tf.truncated_normal(shape=[self.n_out,  self.n_hid], dtype=tf.float64), name="W_yv")
            
            self.U_ss = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="U_ss")
            
            
            # Attention weights
            self.U_se = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="U_se")
            self.U_he = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_hid], dtype=tf.float64), name="U_he")
            self.v_e  = tf.Variable(tf.truncated_normal(shape=[self.n_hid, 1], dtype=tf.float64), name="v_e")
            
            
            # Output weights
            self.U_so = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_out], dtype=tf.float64), name="U_so")
            self.C_Co = tf.Variable(tf.truncated_normal(shape=[self.n_hid,  self.n_out], dtype=tf.float64), name="C_Co")
            
            
            self.queries = None
            self.keys    = None
            self.values  = None
            

            self.h = self.f_encoder()
            self.s = self.f_decoder()
            
            self.out = tf.scan(self.f_out, self.s, initializer=(tf.constant(0, shape=[self.n_out], dtype=tf.float64)))
            
            # loss f()
            self.loss = -tf.reduce_mean(
                self.y * tf.log(self.out + self.epsilon) + (1. - self.y) * tf.log(1. - self.out + self.epsilon)
            )
            
            # self.loss = tf.losses.sigmoid_cross_entropy(self.y, self.out)
            
            # optimizer
            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(self.loss)
            
            
            # var init
            self.tf_init = tf.global_variables_initializer()
            
            # saver
            save_dict = {
                "W_xq": self.W_xq,
            }
            
            self.tf_saver = tf.train.Saver(save_dict)
            
            # weights histogram
            tf.summary.histogram("W_xq", self.W_xq)
            self.summaries = tf.summary.merge_all()
            
            
        self.init_sess()
    
    
    
    def f_self_attention(self, _out, query_t):
        """
        needed to be done in one methode, because
        reshape with shape ? is not possible
        
        IMPORTANT
        self.queries, self.keys, self.values needs to be set
        before you call this method
        
        """
        
        query_t = tf.reshape(query_t, [query_t.shape[0], 1])
        
        # self attention score ...
        score_t = tf.matmul(self.keys, query_t)
        
        # only here to get more stable gradients
        shape_sqrt = tf.sqrt(tf.cast(query_t.shape[0], dtype=tf.float64))
        score_t = tf.truediv(score_t, shape_sqrt)
        
        # softmax
        score_t = tf.nn.softmax(score_t, axis=0)
        # ... self attention score
        
        # self attention value ...
        softmax_value_t = tf.multiply(self.values, score_t)
        sum_value_t = tf.reduce_sum(softmax_value_t, axis=0)
        
        return sum_value_t
    
    def f_encoder(self):
        
        self.queries = tf.matmul(self.x, self.W_xq)
        self.keys    = tf.matmul(self.x, self.W_xk)
        self.values  = tf.matmul(self.x, self.W_xv)
        
        h = tf.scan(self.f_self_attention, self.queries)
        
        return h # tf.tanh( tf.matmul(h, self.U_hh) )
        
    def attention_e(self, s_t):
        
        mul  = tf.matmul
        tanh = tf.tanh
        
        return mul( tanh(mul(s_t, self.U_se) + mul(self.h, self.U_he)), self.v_e )
    
    def attention_alpha(self):
        return tf.nn.softmax(self.e, axis=0)
        
    def attention_context(self):
        return tf.reduce_sum(tf.multiply(self.alpha, self.h), 0)
    
    def f_decoder(self):
        
        # teacher forcing
        zeros = tf.constant(0.0, shape=[1, self.y.shape[1]], dtype=tf.float64)
        y = self.y[0:-1]
        y = tf.concat([zeros, y], axis=0)
        
        self.queries = tf.matmul(y, self.W_yq)
        self.keys    = tf.matmul(y, self.W_yk)
        self.values  = tf.matmul(y, self.W_yv)
        
        s = tf.scan(self.f_self_attention, self.queries)
        
        return s # tf.tanh( tf.matmul(s, self.U_ss) )
        
    def f_out(self, _out, s_t):
        
        mul = tf.matmul
        sig = tf.sigmoid
        
        s_t = tf.expand_dims(s_t, 0)
        
        self.e = self.attention_e(s_t)
        self.alpha = self.attention_alpha()
        C_t = self.attention_context()
        
        C_t = tf.expand_dims(C_t, 0)
        
        out = sig( mul(C_t, self.C_Co) + mul(s_t, self.U_so) )
        
        return tf.reshape(out, [out.shape[1]])
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        