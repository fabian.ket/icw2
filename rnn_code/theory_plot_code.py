import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import truncnorm

def gaussian(x, mean=0.0, sigma=1.0):
    return 1 / np.sqrt(2*np.pi*sigma**2) * np.exp(-(x-mean)**2 / (2*sigma**2))

def plot_truncated_gaussian_samples(mean=0.0, sigma=1.0):
    x = np.linspace(-5, 5, 101)

    gaussian_sampels = np.random.normal(mean, sigma, 10000)
    truncated_gaussian_samples = truncnorm.rvs(-2*sigma, 2*sigma, size=10000)

    fig = plt.figure(figsize=(10, 10))
    plt.plot(x, gaussian(x, mean=mean, sigma=sigma), "r-", linewidth=3)
    _ = plt.hist(gaussian_sampels, density=True, color="b")
    _ = plt.hist(truncated_gaussian_samples, density=True, color="g")